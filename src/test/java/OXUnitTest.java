/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Tramboliko
 */
public class OXUnitTest {

    public OXUnitTest() {
    }

    @Test
    public void testGetNameO() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        assertEquals('O', o.getName());
    }

    @Test
    public void testGetNameX() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        assertEquals('O', o.getName());
    }

    @Test
    public void testCheckInputFalse() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        assertEquals(false, table.checkInputRowCol(4, 4));
    }

    @Test
    public void testCheckInputTrue() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        assertEquals(true, table.checkInputRowCol(2, 2));
    }

    @Test
    public void testCheckRow0Win() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkWin());
    }

    @Test
    public void testCheckRow1Win() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        assertEquals(true, table.checkWin());
    }

    @Test
    public void testCheckRow2Win() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    
    @Test
    public void testCheckCol0Win() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());
    }
    
    @Test
    public void testCheckCol1Win() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.setRowCol(3, 2);
        assertEquals(true, table.checkWin());
    }
    
    @Test
    public void testCheckCol2Win() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 3);
        table.setRowCol(2, 3);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
