/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tramboliko
 */
import java.util.*;

public class Game {

    Scanner kb = new Scanner(System.in);
    private Table table;
    private int row, col;
    private Player x, o;

    public Game() {
        x = new Player('X');
        o = new Player('O');
    }

    public void startGame() {
        table = new Table(o, x);
    }

    public void showWelcome() {
        System.out.println("Welcome to XO Game");
    }

    public void showTable() {
        char data[][] = table.getData();
        System.out.println("  1 2 3");
        for (int i = 0; i < 3; i++) {
            System.out.print((i + 1));
            for (int j = 0; j < 3; j++) {
                System.out.print(" " + data[i][j]);
            }
            System.out.println("");
        }
    }

    public void showTurn() {
        System.out.println("Turn " + table.getCurrentPlayer().getName());
    }

    public void showWin() {
        Player player = table.getWinner();
        this.showTable();
        if (table.getWinner() == null) {
            System.out.println("Draw");
        } else {
            System.out.println("Player " + player.getName() + " Win");
        }
        System.out.println("O: Win: " + o.getWin() + " Lose: " + o.getLose() + " Draw: " + o.getDraw());
        System.out.println("X: Win: " + x.getWin() + " Lose: " + x.getLose() + " Draw: " + x.getDraw());
    }

    private static void showBye() {
        System.out.println("Bye bye ...");
    }

    public boolean inputRowCol() {
        System.out.print("Please input row/col : ");
        row = kb.nextInt();
        col = kb.nextInt();
        if (table.checkInputRowCol(row, col)) {
            table.setRowCol(row, col);
            return true;
        } else {
            return false;
        }
    }

    public boolean inputToContinue() {
        System.out.print("Continue? (y/n): ");
        String yn = kb.next();
        if (yn.equals("y")) {
            return true;
        }
        return false;
    }

    public void run() {
        showWelcome();
        do {
            startGame();
            runOne();
        } while (inputToContinue());
        showBye();
    }

    public void runOne() {
        while (true) {
            this.showTable();
            showTurn();
            if (inputRowCol()) {
                if (table.checkWin()) {
                    showWin();
                    return;
                } else if (table.checkDraw()) {
                    showWin();
                    return;
                }
            }

        }
    }

}
